﻿using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Xml;

namespace pkg_rip_tool
{
    public class Updates
    {
        public string version;
        public long size;
        public string hash;
        public string link;
        public string firmware;
        public string title;
        public string titleid;
    }

    public class Get_Updates
    {
        const string U_A = "I'll grab this... -D";

        public Updates[] Fetch(string titleid)
        {
            byte pkgCount = 0;
            string url = "https://a0.ww.np.dl.playstation.net/tpl/np/" + titleid + "/" + titleid + "-ver.xml";

            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; }); // Disable SSL certificate validation

            HttpWebRequest requestCount = (HttpWebRequest)HttpWebRequest.Create(url);
            requestCount.UserAgent = U_A;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.UserAgent = U_A;

            try
            {
                using (WebResponse responseCount = requestCount.GetResponse())
                using (Stream responseStreamCount = responseCount.GetResponseStream())
                using (XmlTextReader readerCount = new XmlTextReader(responseStreamCount))
                {
                    while (readerCount.Read())
                    {
                        switch (readerCount.NodeType)
                        {
                            case XmlNodeType.Element:
                                if (readerCount.Name.Contains("package"))
                                    pkgCount++;
                                break;
                        }
                    }
                }

                Updates[] updates = new Updates[pkgCount];
                for (int i = 0; i < updates.Length; i++)
                    updates[i] = new Updates();

                ushort position = 0;
                using (WebResponse response = request.GetResponse())
                using (Stream responseStream = response.GetResponseStream())
                using (XmlReader reader = XmlReader.Create(responseStream))
                {
                    while (reader.Read())
                    {
                        switch (reader.NodeType)
                        {
                            case XmlNodeType.Element:
                                while (reader.MoveToNextAttribute())
                                {
                                    if (position < pkgCount)
                                    {
                                        switch (reader.Name)
                                        {
                                            case "titleid":
                                                if (string.IsNullOrEmpty(updates[0].title))
                                                    updates[position].titleid = reader.Value;
                                                break;
                                            case "version":
                                                updates[position].version = reader.Value;
                                                if (updates[position].version.StartsWith("0"))
                                                    updates[position].version = updates[position].version.Remove(0, 1);
                                                break;
                                            case "size":
                                                updates[position].size = Convert.ToInt64(reader.Value);
                                                break;
                                            case "sha1sum":
                                                updates[position].hash = reader.Value.ToUpper();
                                                break;
                                            case "url":
                                                updates[position].link = reader.Value;
                                                break;
                                            case "ps3_system_ver":
                                                updates[position].firmware = reader.Value.Remove(0, 1).Remove(4, 2);
                                                position++;
                                                break;
                                        }
                                    }
                                }
                                break;

                            case XmlNodeType.Text:
                                if (string.IsNullOrEmpty(updates[0].title))
                                    updates[0].title = reader.Value;
                                break;
                        }
                    }
                }

                for (int i = 0; i < (pkgCount); i++)
                {
                    updates[i].title = updates[0].title;
                    updates[i].titleid = updates[0].titleid;
                }

                return updates;
            }
            
            catch (Exception ex)
            {
                if (ex is IndexOutOfRangeException || ex is XmlException)
                {
                    Updates[] noUpdates = new Updates[1];
                    noUpdates[0] = new Updates();

                    noUpdates[0].titleid = titleid;
                    noUpdates[0].title = "No updates found for this TitleID.";
                    noUpdates[0].version = String.Empty;
                    noUpdates[0].firmware = String.Empty;
                    noUpdates[0].size = 0;
                    noUpdates[0].hash = String.Empty;
                    noUpdates[0].link = String.Empty;

                    return noUpdates;
                }

                else if (ex is WebException)
                {
                    Updates[] invalidTitle = new Updates[1];
                    invalidTitle[0] = new Updates();

                    invalidTitle[0].titleid = titleid;
                    invalidTitle[0].title = "This TitleID does not exist.";
                    invalidTitle[0].version = String.Empty;
                    invalidTitle[0].firmware = String.Empty;
                    invalidTitle[0].size = 0;
                    invalidTitle[0].hash = String.Empty;
                    invalidTitle[0].link = String.Empty;

                    return invalidTitle;
                }

                throw;
            }
        }
    }
}
