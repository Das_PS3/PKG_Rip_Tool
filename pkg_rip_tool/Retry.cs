﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace pkg_rip_tool
{
    public static class Retry
    {
        public static void Do(Action action, TimeSpan? retryInterval = null, int retryCount = 3)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, retryInterval, retryCount);
        }

        public static T Do<T>(Func<T> action, TimeSpan? retryInterval = null, int retryCount = 3)
        {
            if (retryInterval == null)
                retryInterval = TimeSpan.FromSeconds(30);

            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    Thread.Sleep(retryInterval.Value);
                }
            }

            return default(T);
            //throw new AggregateException(exceptions);
        }
    }
}
