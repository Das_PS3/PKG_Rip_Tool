﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;

namespace pkg_rip_tool
{
    class Program
    {
        private const string UPDATES_FILE = "updates.txt";
        private const string JOBS_FILE = "jobs.txt";
        private const string DB_FILE = "db.csv";
        private const string DOWNLOADS_FOLDER = @"target\";

        private const string U_A = "I'll grab this... -D";

        static void Main(string[] args)
        {
            if (File.Exists(JOBS_FILE))
            {
                ProcessLinksFile(JOBS_FILE);
            }

            if (File.Exists(DB_FILE))
            {
                ProcessLinksFile(DB_FILE);
            }

            if (!File.Exists(JOBS_FILE) && !File.Exists(DB_FILE))
            {
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                Console.WriteLine(string.Format("Error. Neither {0} nor {1} exist. No actions will be taken.", JOBS_FILE, DB_FILE));

                Console.WriteLine("{0} {1} by {2}, written for catalinnc." + Environment.NewLine, fvi.ProductName, fvi.ProductVersion, fvi.CompanyName);
            }
        }

        static void ProcessLinksFile(string filename)
        {
            string[] filenameContents = File.ReadAllLines(filename);

            if (!Directory.Exists(DOWNLOADS_FOLDER))
                Directory.CreateDirectory(DOWNLOADS_FOLDER);

            for (int i = 0; i < filenameContents.Length; i++)
            {
                string link = Regex.Match(filenameContents[i], @"http://.*\.pkg", RegexOptions.IgnoreCase).Value;

                if (string.IsNullOrWhiteSpace(link))
                    continue;

                string titleID = Regex.Match(link, @"/[A-Z]{4}\d{5}").Value.Substring(1);

                Get_Updates updateChecker = new Get_Updates();
                var updates = updateChecker.Fetch(titleID);

                if (updates.Length > 0 && !string.IsNullOrWhiteSpace(updates[0].link))
                    for (int x = 0; x < updates.Length; x++)
                    {
                        LogUpdateLink(updates[x].link);

                        string updatePKGFilename = Path.GetFileName(updates[x].link);

                        if (File.Exists(DOWNLOADS_FOLDER + updatePKGFilename + ".header") && File.Exists(DOWNLOADS_FOLDER + updatePKGFilename + ".footer"))
                        {
                            Console.WriteLine("Skipping update..." + Environment.NewLine + updates[x].link);
                            continue;
                        }

                        Console.WriteLine("Processing update..." + Environment.NewLine + updates[x].link);

                        long? updatePKGFilesize = Retry.Do(() => GetFileSize(updates[x].link));

                        if (updatePKGFilesize.HasValue)
                        {
                            Retry.Do(() => GetFileRange(updates[x].link, DOWNLOADS_FOLDER + updatePKGFilename + ".header", 0, 255));
                            Retry.Do(() => GetFileRange(updates[x].link, DOWNLOADS_FOLDER + updatePKGFilename + ".footer", updatePKGFilesize.Value - 256, updatePKGFilesize.Value));

                            Console.WriteLine("{3}Update done! ({0}/{1} updates, {2:0.00}%){3}", x + 1, updates.Length, ((x + 1) * 100) / updates.Length, Environment.NewLine);
                        }
                        else
                            Console.WriteLine("{1}The update {0} returned HTTP ERROR 404: NOT FOUND, skipping...{1}", link, Environment.NewLine);
                    }

                string pkgFilename = Path.GetFileName(link);

                if (File.Exists(DOWNLOADS_FOLDER + pkgFilename + ".header") && File.Exists(DOWNLOADS_FOLDER + pkgFilename + ".footer"))
                {
                    Console.WriteLine("Skipping... " + Environment.NewLine + link);
                    continue;
                }

                Console.WriteLine("Processing... " + Environment.NewLine + link);

                long? pkgFilesize = Retry.Do(() => GetFileSize(link));

                if (pkgFilesize.HasValue)
                {
                    Retry.Do(() => GetFileRange(link, DOWNLOADS_FOLDER + pkgFilename + ".header", 0, 255));
                    Retry.Do(() => GetFileRange(link, DOWNLOADS_FOLDER + pkgFilename + ".footer", pkgFilesize.Value - 256, pkgFilesize.Value));

                    Console.WriteLine("{3}Done! ({0}/{1}, {2:0.00}%){3} [{4}]", i + 1, filenameContents.Length, ((i + 1) * 100) / filenameContents.Length, Environment.NewLine, filename);
                }
                else
                    Console.WriteLine("{1}{0} returned an error, skipping...{1}", link, Environment.NewLine);
            }
        }

        private static void LogUpdateLink(string link)
        {
            string baseDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string updatesFile = Path.Combine(baseDir, UPDATES_FILE);

            if (File.Exists(updatesFile))
            {
                var contents = File.ReadAllLines(updatesFile);
                
                if (contents.Contains(link))
                    return;
            }

            File.AppendAllText(updatesFile, link + Environment.NewLine);
        }

        private static long? GetFileSize(string link)
        {
            HttpWebRequest header = (HttpWebRequest)WebRequest.Create(link);
            header.Method = "HEAD"; // Defaults to GET
            header.UserAgent = U_A;

            using (HttpWebResponse headerResponse = (HttpWebResponse)header.GetResponse())
                return headerResponse.ContentLength;
        }

        private static void GetFileRange(string link, string targetFilename, long rangeStart, long rangeEnd)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(link);
            request.UserAgent = U_A;
            request.AddRange(rangeStart, rangeEnd);

            byte[] readBuffer = new byte[rangeEnd - rangeStart];

            using (WebResponse response = request.GetResponse())
            using (Stream data = response.GetResponseStream())
            using (BinaryWriter writer = new BinaryWriter(File.Open(targetFilename, FileMode.Create, FileAccess.Write)))
            {
                writer.BaseStream.Position = writer.BaseStream.Length;

                int readBytesCount;

                while ((readBytesCount = data.Read(readBuffer, 0, readBuffer.Length)) > 0)
                    writer.Write(readBuffer, 0, readBytesCount);
            }

            FileInfo fi = new FileInfo(targetFilename);
            if (fi.Length != 256)
                throw new Exception(string.Format("Incorrect file size (expected {0} bytes, obtained {1}{2}", readBuffer.Length, fi.Length, Environment.NewLine));
        }
    }
}
